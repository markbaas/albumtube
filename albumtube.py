import urllib
import json
import pafy
from distutils import dir_util
import os, os.path
from mutagenx.oggvorbis import OggVorbis
from mutagenx.easyid3 import EasyID3
from mutagenx.easymp4 import EasyMP4
import tempfile
import subprocess
from unidecode import unidecode
import argparse
import sys
import csv

sys.path.append(os.path.join(os.path.dirname(__file__), 'libs'))
import spotipy


STATUS_EXISTS = (1001, "Track already downloaded")
STATUS_DOWNLOAD_FAILED = (1002, "Could not download track")
STATUS_COULD_NOT_FIND = (1003, "Could not find track on youtube")
STATUS_SUCCESS = (1000, "Success!")

ALBUM_LIST = 2000
TRACK_LIST = 2001
SPOTIFY_LIST = 2002
DEEZER_LIST = 2003

def simple_string(str):
    return unidecode(str.lower())

def create_path(path):
    dir_util.mkpath(os.path.dirname(path))
    
def convert_file(src_path, dest_path):
    subprocess.call(['avconv', '-v', 'quiet', '-i', src_path, dest_path])

def write_tracklist(dest_path, lines):
    dir_util.mkpath(os.path.dirname(dest_path))
    with open(dest_path, 'w') as csvfile:
        csvwriter = csv.writer(csvfile, delimiter=';', quotechar='"')
        for line in lines:
            csvwriter.writerow(line)
    
class iTunesLibrary:
    def __init__(self):
        pass
        
    def get_tracks(artist_name, album_name):
        artists = itunes.search_artist(artist_name)
        if not artists: return []

        artist = artists[0]

        tracks = []
        for album in artist.get_albums():
            if album.get_name().lower() == album_name.lower():
                for track in album.get_tracks():
                    tracks.append(track)
        return tracks
        
class SpotifyLibrary:
    def __init__(self):
        self.sp = spotipy.Spotify()
    
    def get_tracks_from_playlis(self, id):
        sp.user_playlist(username, playlist['id'])
        return self.convert_obj(results['tracks'])

    def get_tracks_by_spotify_id(self, pid):
        album = self.sp.album(pid)
        return self.convert_obj(album['tracks']['items'], album)
        
    def get_tracks(self, artist_name, album_name):
        albums = self.sp.search(q=album_name, type='album')['albums']['items']
        albums.extend(self.get_albums(artist_name))
        for short_album in albums:
            album = self.sp.album(short_album['id'])
            artist = None
            for artisti in album['artists']:
                if simple_string(artisti['name']) == simple_string(artist_name):
                    artist = artisti
                    break
            if artist:
                return self.convert_obj(album['tracks']['items'], album)
        
    def get_albums(self, artist_name):
        artists = self.sp.search(q=artist_name, type='artist')['artists']['items']
        albumsx = []
        albums = []
        names = []
        for artist in artists:
            results = self.sp.artist_albums(artist['id'])
            albumsx.extend(results['items'])
            while results['next']:
                results = self.sp.next(results)
                albumsx.extend(results['items'])
        for album in albumsx:
            if album['name'] in names:
                continue
            albums.append(self.sp.album(album['id']))
            names.append(album['name'])
        return albums

    def convert_obj(self, items, album):
        # convert album
        new_album = {
            'name': album['name']  
        }
        
        # convert tracks
        new_items = []
        for item in items:
            new_items.append({ 
                'artist': item['artists'][0]['name'],
                'name': item['name'],
                'track_number': "{0:02d}".format(item['track_number']),
                'duration': item['duration_ms']/1000,
                'album': new_album
            })

        return new_items


class TrackDownloader:
    def __init__(self, track, download_path, path_without_artist=False):
        self.track = track
        if not 'album_artist' in self.track:
            self.track['album_artist'] = self.track['artist']
        self.download_path = download_path
        self.dest_file = self.get_dest_file(path_without_artist=path_without_artist)

    def get_dest_file(self, path_without_artist=False):
        dest_file = "{0} - {1}".format(self.track['track_number'], self.track['name'])
        dest_file = dest_file.replace("/", "-").replace("\\", "-")
        if path_without_artist:
            dest_path = os.path.join(self.download_path, self.track['genre'], self.track['album']['name'], dest_file)
        else:
            dest_path = os.path.join(self.download_path, self.track['genre'], self.track['album_artist'], self.track['album']['name'], dest_file)

        return dest_path
        
    def download(self):
        result = {'status': 'success', 'reason': STATUS_SUCCESS}
    
        # Get youtube link
        if not 'link' in self.track or not self.track['link']:
            data = self.get_youtube_link()
            if not data:
                result['status'] = 'failed'
                result['reason'] = STATUS_COULD_NOT_FIND
                self.track['link'] = ''
                return result
            
        if os.path.exists(self.dest_file + ".mp3"):
            result['status'] = 'success'
            result['reason'] = STATUS_EXISTS
            return result

        # Download stream    
        downloaded_file = self.download_stream()
        if not downloaded_file:
            result['status'] = 'failed'
            result['reason'] = STATUS_DOWNLOAD_FAILED
            return result
            
        # Set metadata
        self.set_metadata()
        
        return result

    def get_youtube_link(self):
        search = self.track['artist'] + " - " + self.track['name'].split("-")[0]    
        search = unidecode(search.replace(".", " "))
        url = "http://gdata.youtube.com/feeds/api/videos?vq={0}&racy=include&orderby=relevance&alt=jsonc&v=2".format(urllib.parse.quote_plus(search))

        r = urllib.request.urlopen(url)
        data = json.loads(r.read().decode('utf-8'))

        max_score = 0
        best_match = None
        position_score = 1.2
        matches = []

        #print("Youtube: searched: {0}".format(search),)

        if not "items" in data['data']:
            return None

        for item in data['data']['items']:
            matches.append(item['title'])

            # Get delta score
            if "duration" in self.track:
                delta = abs(item['duration'] - self.track['duration'])
                if delta == 0:
                    delta_score = 1000000
                else:
                    delta_score = 1 / delta
            else:
                delta_score = 1

            # Get title score
            keywords_found = 0
            title = item['title'].lower()
            for word in title.split():
                if word in search.lower():
                    keywords_found += 1
                else:
                    keywords_found -= 1

            if keywords_found == 0:
                title_score = 0
            else:
                title_score = (keywords_found / len(search.split()))
            if  title_score <= 0.5: title_score = 0

            # get position score
            position_score -= 0.1

            # Get total_score and max
            total_score = (delta_score * title_score * position_score)

            if total_score > max_score:
                max_score = total_score
                best_delta_score = delta_score
                best_title_score = title_score
                best_match = item

        if not best_match:    
            return

        item = best_match
        #print("{0} {1}".format(item['title'], item['duration']))

        self.track['link'] = item['player']['default']

        return {'url': item['player']['default'], 'title': item['title'], 'duration': item['duration']}

    def download_stream(self):
        video = pafy.new(self.track['link'])
        audio = video.getbestaudio()

        self.track['duration'] = video.duration

        create_path(self.dest_file)
        path = self.dest_file + ".mp3"

        if os.path.exists(path):
            return path

        ts = path + "." + audio.extension

        try:
            audio.download(ts, quiet=True)
        except urllib.error.HTTPError:
            return None
            
        convert_file(ts, path)
        os.remove(ts)    
        
        return True

    
    def set_metadata(self):
        audio = EasyID3(self.dest_file + ".mp3")
        audio['title'] = self.track['name']
        audio['artist'] = self.track['artist']
        audio['album'] = self.track['album']['name']
        audio['tracknumber'] = self.track['track_number']
        #audio['date'] = track.get_album().get_release_date()
        audio['genre'] = self.track['genre']
        audio['length'] = str(self.track['duration'])

        audio.save()
    
class AlbumTube:
    def __init__(self, artist, album, genre, download_path, library):
        self.artist = artist
        self.album = album
        self.genre = genre
        self.download_path = download_path
        self.library = library
        self.tracks = None
    
    @classmethod
    def from_spotifyid(cls, pid, genre, download_path, library):
        
        tracks = library.get_tracks_by_spotify_id(pid)
        artist = tracks[0]['artist']
        album = tracks[0]['album']['name']
        tube = cls(artist, album, genre, download_path, library)
        tube.tracks = tracks
        return tube
    
    def download_album(self):
        if not self.tracks:
            tracks = self.library.get_tracks(self.artist, self.album)
        else:
            tracks = self.tracks

        tracklist = [['Artist', 'Album', 'Genre', 'Title', 'Link']]
        status = 'completed'
        
        for track in tracks:
            track['genre'] = self.genre
            downloader = TrackDownloader(track, self.download_path)
            result = downloader.download()
            print("{0} - {1}: {2} {3}".format(track['track_number'], track['name'], result['status'], result['reason'][1]))
            
            if not result['status'] == 'success':
                status = 'failed'
            
            tr = downloader.track
            tracklist.append([tr['artist'], tr['album']['name'], tr['genre'], tr['name'], tr['link']])

        write_tracklist(os.path.join(status, self.artist + ' - ' + self.album + '.csv'), tracklist)
            
            
def process_albumfile(csvfile, download_path):
    import sys, csv

    with open(csvfile) as csvfile:
        csvreader = csv.reader(csvfile, delimiter=';', quotechar='"')
        headers = csvreader.__next__()
        for row in csvreader:
            artist = row[headers.index('Artist')]
            album = row[headers.index('Album')]
            genre = row[headers.index('Genre')]
            library = SpotifyLibrary()
            downloader = AlbumTube(artist, album, genre, download_path, library)
            downloader.download_album()
            
def process_trackfile(csvf, download_path):

    with open(csvf) as csvfile:
        csvreader = csv.reader(csvfile, delimiter=';', quotechar='"')
        headers = csvreader.__next__()
        track_numbers = {}
        tracklist = [['Artist', 'Album', 'Genre', 'Title', 'Link']]
        status = 'completed'
        
        # check if all same artist
        rows = list(csvreader)
        artists = sorted([row[headers.index('Artist')] for row in rows])
        path_without_album = artists[0] != artists[-1]
        
        for row in rows:
            artist = row[headers.index('Artist')]
            album = row[headers.index('Album')]
            genre = row[headers.index('Genre')]
            title = row[headers.index('Title')]
            try:
                link = row[headers.index('Link')]
            except:
                link = ''

            if not album in track_numbers:
                track_numbers[album] = 0
            track_numbers[album] += 1
            
            track = {'name': title,
                     'artist': artist,
                     'album_artist': artist,
                     'album': {'name': album},
                     'genre': genre,
                     'track_number': "{0:02d}".format(track_numbers[album]),
                     'link': link
                     }
            downloader = TrackDownloader(track, download_path, path_without_artist=path_without_album)
            result = downloader.download()
            print("{0} - {1}: {2} {3}".format(track['track_number'], track['name'], result['status'], result['reason'][1]))
            
            tr = downloader.track
            tracklist.append([tr['artist'], tr['album']['name'], tr['genre'], tr['name'], tr['link']])
        
        write_tracklist(os.path.join(status, os.path.basename(csvf)), tracklist)

def process_spotifylist(csvfile, download_path):
    import sys, csv

    with open(csvfile) as csvfile:
        csvreader = csv.reader(csvfile, delimiter=';', quotechar='"')
        headers = csvreader.__next__()
        for row in csvreader:
            pid = row[headers.index('Spotify')]
            genre = row[headers.index('Genre')]
            library = SpotifyLibrary()
            downloader = AlbumTube.from_spotifyid(pid, genre, download_path, library)
            downloader.download_album()

def query_albums(artist_name):
    library = SpotifyLibrary()
    albums = library.get_albums(artist_name)
    for album in albums:
        print("{0} ({1})".format(album['name'], album['type']))

def detect_list_type(l):
    if l.endswith('.csv'):
        with open(l) as csvfile:
            csvreader = csv.reader(csvfile, delimiter=';', quotechar='"')
            headers = csvreader.__next__()
            if 'Spotify' in headers:
                return SPOTIFY_LIST 
            elif 'Title' in headers:
                return TRACK_LIST
            else:
                return ALBUM_LIST
    else:
        raise NotImplementedError('Currently you can only provide a csv file')

if __name__ == "__main__":
    incoming_path = os.path.join(os.path.dirname(__file__), "incoming")
    completed_path = os.path.join(os.path.dirname(__file__), "completed")
    failed_path = os.path.join(os.path.dirname(__file__), "failed")
    download_path = os.path.join(os.path.dirname(__file__), "downloads")
    
    parser = argparse.ArgumentParser(description = 'Youtube music download helper')
    #parser.add_argument('dlist', help = 'Get list of albums,tracks from csv or online playlist (i.e. spotify, deezer)')
    #parser.add_argument('download_path', help = 'Path to download files to')
    parser.add_argument('--query', dest='query', help = 'Search albums from artist specified')
    args = parser.parse_args()
    
    if args.query:
        query_albums(args.query)
    else:
        for f in os.listdir(incoming_path):
            path = os.path.join(incoming_path, f)
            if f.startswith("."): continue
            
            print(path)
            list_type = detect_list_type(path)
            if list_type == ALBUM_LIST:
                process_albumfile(path, download_path)
            elif list_type == TRACK_LIST:
                process_trackfile(path, download_path)
            elif list_type == SPOTIFY_LIST:
                process_spotifylist(path, download_path)
        


if __name__ == "__xmain__":

    parser = argparse.ArgumentParser(description = 'Youtube music download helper')
    parser.add_argument('dlist', help = 'Get list of albums,tracks from csv or online playlist (i.e. spotify, deezer)')
    parser.add_argument('download_path', help = 'Path to download files to')
    parser.add_arguments('--query', dest='query', help = 'Search albums from artist specified')
    args = parser.parse_args()
    print(args)
    if args.query:
        query_albums(args.query)
    else:
    
        list_type = detect_list_type(args.dlist)
    
        if list_type == ALBUM_LIST:
            process_albumfile(args.dlist, args.download_path)
        elif list_type == TRACK_LIST:
            process_trackfile(args.dlist, args.download_path)

    

