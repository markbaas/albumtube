Descriptions
============
Download helper for downloading complete albums from youtube. Should be completely legal, otherwise inform and I will take down the project.

Installation
============
Install Python >= 3.2

Create the following folders at the same level as albumtube.py:
- incoming
- completed
- downloaded
- failed

Requirements
============
With pip or however you wish install the followig packages:
- pafy
- mutagenx
- unidecode
- spotipy

How to use?
===========
There are three ways to specify which music your want to get:

Complete album
--------------
1. Create a CSV with the following headers (first row): Artist, Album, Genre.
2. Add desired albums and put the file in incoming. 
3. Start albumtube (python3 albumtube.py)
4. Get your files from download directory

List of tracks
--------------
1. Create a CSV with the following headers (first row): Artist, Album, Genre, Title, Link (optional)
2. Link is optional, however if albumtube wasn't able to find the file, you can put a link to override
3. Add desired albums and put the file in incoming. 
4. Start albumtube (python3 albumtube.py)
5. Get your files from download directory

Spotifyuri
----------
1. Create a CSV with the following headers (first row): Spotify, Genre
2. ... 
3. Start albumtube (python3 albumtube.py)
4. Get your files from download directory

Finally, files that have failed are saved in the failed directory and succesful onces in completed. You can fix the links in the failed files and then put them in incoming again.